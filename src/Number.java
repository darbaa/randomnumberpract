import java.util.Random;

public class Number {

    private static Random rnd = new Random();

    private int value;

    public int getValue() {
        return value;
    }

    public Number(int value) {
        this.value = rnd.nextInt(100);


    }

    @Override
    public String toString() {
        return "Number{" +
                "value=" + value +
                '}';
    }
}
